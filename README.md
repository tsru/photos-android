# Photos - Android Application
A Robust Photo album android application that helps you to organize your photos with search tag functionalities.

* Application follows material design guidelines with a beautiful custom made grid view.
* Application is optimized to load large bitmaps efficiently while maintaining a smaller memory footprint.