package xyz.nurat.photoalbum;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Authors: Tarun Sreenathan & Xiaochen Li
 */

public class TagAddActivity extends AppCompatActivity {
    public static final String ALBUM_INDEX = "albumIndex";
    public static final String PHOTO_INDEX = "photoIndex";
    public static final String PHOTO_NAME = "photoName";
    private ReadWriter rw;
    private String type = "person";
    private ListView listView;
    private int albumIndex;
    private int photoIndex;
    private String photoName;

    protected boolean person = true;
    private EditText valueField;
    private ArrayList<Tag> tags;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addtag);

        context = this;
        rw = ReadWriter.getInstance(context);

        valueField = (EditText)findViewById(R.id.tagValue);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            albumIndex = bundle.getInt(ALBUM_INDEX);
            photoIndex = bundle.getInt(PHOTO_INDEX);
            photoName = bundle.getString(PHOTO_NAME);
        }

        setTitle(photoName);
        tags = rw.getTags(albumIndex, photoIndex);

        view();
    }


    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.radioPerson:
                if (checked)
                    person = true;
                    type = "person";
                break;
            case R.id.radioLocation:
                if (checked)
                    person = false;
                    type = "location";

                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }


    public void addTag(View view){
        String value = valueField.getText().toString();

        if(value == null || value.length() == 0){
            Messages.showDFMessage("Missing Field","Missing Field","Tag Value Required",getFragmentManager());
            return;

        }
        if (rw.tagExists(albumIndex, photoIndex, type, value))
            Messages.showDFMessage("Tag Exists", value, "Tag Already Exists", getFragmentManager());

        else{
            rw.addTag(albumIndex,photoIndex,type,value);
        }


        valueField.getText().clear();
        view();

    }

    public void view(){
        listView = (ListView)findViewById(R.id.tagList);
        TagsAdapter ta = new TagsAdapter(tags, this);
        listView.setAdapter(ta);
    }

    public class TagsAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<Tag> list = new ArrayList<Tag>();
        private Context context;

        public TagsAdapter(ArrayList<Tag> list, Context context) {
            this.list = list;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int pos) {
            return list.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;

            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.activity_tagscell, null);
            }

            //View Text
            TextView itemText = (TextView)view.findViewById(R.id.string_text_tag);
            itemText.setText(list.get(position).getType() + " : "+ list.get(position).getValue());

            //Handle buttons and add onClickListeners
            Button deleteBtn = (Button)view.findViewById(R.id.delete_btn_tag);

            deleteBtn.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    rw.removeTag(albumIndex,photoIndex,position);
                    view();
                }
            });

            return view;
        }
    }


}
