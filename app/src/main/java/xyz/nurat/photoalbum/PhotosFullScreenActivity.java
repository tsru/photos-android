package xyz.nurat.photoalbum;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Authors: Tarun Sreenathan & Xiaochen Li
 */

public class PhotosFullScreenActivity extends AppCompatActivity {

    public static final String PHOTO_INDEX = "photoIndex";
    public static  final String ALBUM_INDEX = "albumIndex";
    public static  final String ALBUM_NAME = "albumName";
    private int albumIndex;
    private int photoIndex;
    private String albumName;
    private ImageView fullImage;
    private ArrayList<Photo> photos;
    private ReadWriter rw;
    Context context;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreenphotos);

        context = this;
        rw = ReadWriter.getInstance(context);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            albumIndex = bundle.getInt(ALBUM_INDEX);
            albumName = bundle.getString(ALBUM_NAME);
            photoIndex = bundle.getInt(PHOTO_INDEX);
        }
        photos = rw.getPhotos(albumIndex);
        if(photoIndex == rw.getPhotos(albumIndex).size() -1){
            Button next =  (Button) findViewById(R.id.next);
            next.setEnabled(false);
        }
        if(photoIndex==0){
            Button prev =  (Button) findViewById(R.id.prev);
            prev.setEnabled(false);
        }
        view();
    }

    @Override
    public void onResume() {
        super.onPause();
        view();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public void delete(View view){
        new AlertDialog.Builder(context)
                .setTitle(photos.get(photoIndex).getFileName())
                .setMessage("Are you sure you want to delete ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        rw.removePhoto(albumIndex, photoIndex);
                        finish();
                    }})

                .setNegativeButton(android.R.string.no, null).show();
    }

    public void addTag(View view){
        Bundle bundle = new Bundle();
        bundle.putInt(TagAddActivity.ALBUM_INDEX, albumIndex);
        bundle.putInt(TagAddActivity.PHOTO_INDEX, photoIndex);
        bundle.putString(TagAddActivity.PHOTO_NAME, photos.get(photoIndex).getFileName());
        Intent intent = new Intent(this, TagAddActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void move(View view){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PhotosFullScreenActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = inflater.inflate(R.layout.activity_popuplist, null);
        alertDialog.setView(convertView);
        alertDialog.setMessage("No other albums found");
        alertDialog.setNegativeButton(android.R.string.cancel,new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id){

            }
        });
        ArrayList<String> names = new ArrayList<String>();
        for(Album a : rw.getAlbums()){

            String s = a.getAlbumName();

            if(a.getAlbumName().equals(albumName));
            else{
                alertDialog.setMessage("Move Photo To");
                names.add(s);
            }

        }
        final AlertDialog ad = alertDialog.create();
        ListView listView = (ListView) convertView.findViewById(R.id.popList);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.activity_popuplistcell, R.id.string_text_popup, names);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id){
                if(position < albumIndex){
                    moveTo(position);
                    ad.dismiss();
                }else {
                    moveTo(position + 1);
                    ad.dismiss();
                }
            }
        });
        ad.show();
    }

   private void moveTo(int position) {

        rw.movePhoto(albumIndex, position, photoIndex);
        finish();

    }

    public void next(View view){
        if(photoIndex < rw.getPhotos(albumIndex).size() -1) {
            photoIndex++;
            if(photoIndex == rw.getPhotos(albumIndex).size() -1){
                Button next =  (Button) findViewById(R.id.next);
                next.setEnabled(false);
            }
            if(photoIndex>0){
                Button prev =  (Button) findViewById(R.id.prev);
                prev.setEnabled(true);
            }
            view();

        }

    }

    public void prev(View view){
        if(photoIndex > 0) {
            photoIndex--;
            if(photoIndex < rw.getPhotos(albumIndex).size() -1){
                Button next =  (Button) findViewById(R.id.next);
                next.setEnabled(true);
            }
            if(photoIndex==0){
                Button prev =  (Button) findViewById(R.id.prev);
                prev.setEnabled(false);
            }
            view();
        }
    }

    public void view(){
        fullImage = (ImageView)findViewById(R.id.fullView);
        fullImage.setImageURI(Uri.parse(photos.get(photoIndex).getUri()));
        TextView tv = (TextView)findViewById(R.id.tagsView);

        StringBuilder builder = new StringBuilder();
        for (Tag value : rw.getTags(albumIndex,photoIndex)) {
            builder.append("#" + value + " ");
        }
        String s = builder.toString();
        if(rw.getTags(albumIndex,photoIndex).isEmpty()){
            tv.setText("No Tags");
        }else{
            tv.setText(s);
        }
        setTitle(photos.get(photoIndex).getFileName());
    }

}
