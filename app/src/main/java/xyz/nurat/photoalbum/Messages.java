package xyz.nurat.photoalbum;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Authors: Tarun Sreenathan & Xiaochen Li
 */

public class Messages {
    public static class DialogF extends DialogFragment {
        public static final String MSG_KEY = "msg_key";
        public static final String TITLE_KEY = "title_key";

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Bundle bundle = getArguments();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(bundle.getString(TITLE_KEY))
                    .setMessage(bundle.getString(MSG_KEY))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });

            return builder.create();
        }


    }
    public static DialogF dFrag = new DialogF();
    public static void showDFMessage(String tag, String title, String message, FragmentManager fm) {
        Bundle bundle = new Bundle();
        bundle.putString(DialogF.TITLE_KEY, title);
        bundle.putString(DialogF.MSG_KEY, message);
        dFrag.setArguments(bundle);
        dFrag.show(fm, tag);
        return;

    }
}
