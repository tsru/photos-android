package xyz.nurat.photoalbum;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Authors: Tarun Sreenathan & Xiaochen Li
 */

public class PhotosActivity extends AppCompatActivity{

    public static final String ALBUM_INDEX = "albumIndex";
    public static final String ALBUM_NAME = "albumName";
    private static final int REQUEST_OPEN_CODE = 1;
    private ArrayList<Photo> photos;
    private GridView grid;
    private TextView none;
    private int albumIndex;
    private String albumName;
    private ReadWriter rw;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);

        context = this;
        rw = ReadWriter.getInstance(context);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            albumIndex = bundle.getInt(ALBUM_INDEX);
            albumName = bundle.getString(ALBUM_NAME);
        }

        none = (TextView)findViewById(R.id.none_photos);

        photos = rw.getPhotos(albumIndex);
        setTitle(albumName);

        view();

    }

    @Override
    public void onResume() {
        super.onPause();
        view();

    }

    private void view() {
        if(photos.isEmpty()){
            none.setText("No Photos Found");
        }else{
            none.setText("");
        }
        grid = (GridView) findViewById(R.id.photosGrid);
        grid.setAdapter(new PhotosAdapter(photos,this));
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id){
                openPhoto(position);
            }

        });
    }

    private void openPhoto(int position) {

        Bundle bundle = new Bundle();
        bundle.putString(PhotosFullScreenActivity.ALBUM_NAME, albumName);
        bundle.putInt(PhotosFullScreenActivity.ALBUM_INDEX, albumIndex);
        bundle.putInt(PhotosFullScreenActivity.PHOTO_INDEX, position);
        Intent intent = new Intent(this, PhotosFullScreenActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchadd, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                addPhoto();
                return true;
            case R.id.action_search:
                search();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void search() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    private void addPhoto() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_OPEN_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_OPEN_CODE & resultCode == RESULT_OK) {

            if (data != null) {

                Uri uri = data.getData();

                Cursor returnCursor =
                        getContentResolver().query(uri, null, null, null, null);

                int nameIndex =
                        returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);

                returnCursor.moveToFirst();

                grantUriPermission(getPackageName(), uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                int takeFlags = data.getFlags();
                takeFlags &= (Intent.FLAG_GRANT_READ_URI_PERMISSION);
                getContentResolver().takePersistableUriPermission(uri, takeFlags);

                rw.addPhoto(uri.toString(), albumIndex, returnCursor.getString(nameIndex));
                view();

            }
        }
    }

    private Bitmap getBitmap(Uri uri) throws IOException{
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        options.inSampleSize = calculateInSampleSize(options, 250, 250);
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor,null,options);
        return bitmap;
    }

    public class PhotosAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<Photo> list = new ArrayList<Photo>();
        private Context context;

        public PhotosAdapter(ArrayList<Photo> list, Context context) {
            this.list = list;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int pos) {
            return list.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ImageView imageView;

            if (convertView == null) {
                imageView = new ImageView(context);
                imageView.setLayoutParams(new GridView.LayoutParams(250, 250));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }

            else
            {
                imageView = (ImageView) convertView;
            }
            Bitmap bitmap = null;
            try {
                bitmap = getBitmap(Uri.parse(list.get(position).getUri()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            imageView.setImageBitmap(bitmap);
            return imageView;
        }
    }
    protected static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

}
