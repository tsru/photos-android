package xyz.nurat.photoalbum;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Authors: Tarun Sreenathan & Xiaochen Li
 */

public class SearchActivity extends AppCompatActivity{

    private ArrayList<Photo> photos;
    private GridView grid;
    private ReadWriter rw;
    private String type = "person";
    protected boolean person = true;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        context = this;
        rw = ReadWriter.getInstance(context);

    }

    public void onRadioButtonClicked(View view){

        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.radioPerson:
                if (checked)
                    person = true;
                type = "person";
                break;
            case R.id.radioLocation:
                if (checked)
                    person = false;
                type = "location";
                break;
        }
    }

    public void searchDo(View view){
        EditText et = (EditText)findViewById(R.id.searchInput);
        String input = et.getText().toString();

        ArrayList<String> hash = new ArrayList<String>();
        for(String s : et.getText().toString().split("\\s+")){
            hash.add(s);
        }

        if(input == null || input.length() == 0){
            Messages.showDFMessage("Missing Fields", "", "Values Required To Search", getFragmentManager());
            return;

        }

        photos = rw.getResult(type, hash);
        et.getText().clear();
        view();

    }


    private void view() {
        if(photos.isEmpty()){
            Toast.makeText(context, "No Photos Found", Toast.LENGTH_SHORT).show();
        }
        grid = (GridView) findViewById(R.id.searchGrid);
        grid.setAdapter(new SearchPhotosAdapter(photos, this));
    }

    private Bitmap getBitmap(Uri uri) throws IOException{
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        return bitmap;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public class SearchPhotosAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<Photo> list = new ArrayList<Photo>();
        private Context context;

        public SearchPhotosAdapter(ArrayList<Photo> list, Context context) {
            this.list = list;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int pos) {
            return list.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ImageView imageView;

            if (convertView == null) {
                imageView = new ImageView(context);
                imageView.setLayoutParams(new GridView.LayoutParams(250, 250));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }

            else
            {
                imageView = (ImageView) convertView;
            }
            Bitmap bitmap = null;
            try {
                bitmap = getBitmap(Uri.parse(list.get(position).getUri()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            imageView.setImageBitmap(bitmap);
            return imageView;
        }
    }
}
