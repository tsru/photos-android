package xyz.nurat.photoalbum;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Authors: Tarun Sreenathan & Xiaochen Li
 */

public class ReadWriter {

    private static ReadWriter instance = null;

    private ArrayList<Album> albums;
    public static final String url = "data.dat";
    Context context;

    protected ReadWriter(Context context){
        this.context = context;

            File file = new File(context.getFilesDir() + File.separator + url);
            if(file.exists()){
                Read();
            }else{
                File des = context.getFilesDir();
                if(des.isDirectory()){
                    for(File in : des.listFiles()){
                        in.delete();
                    }
                }
                albums = new ArrayList<Album>();
                Write();
            }
        return;

    }

    public static ReadWriter getInstance(Context context){
        if(instance == null){
            instance = new ReadWriter(context);
        }
        return instance;
    }

    public ArrayList<Album> getAlbums(){
        return albums;
    }

    public void addAlbum(String album){
        albums.add(new Album(album));
        Write();
    }

    public void removeAlbum(int ai){

        albums.remove(ai);
        Write();
    }

    public void renameAlbum(int ai, String name){

        Album album = albums.get(ai);
        album.setAlbumName(name);
        Write();

    }

    public void addPhoto(String uri, int ai, String fileName){

        Album a = albums.get(ai);
        a.addPhoto(new Photo(uri, fileName));
        Write();

    }

    public void removePhoto(int ai, int pi){

        Album a = albums.get(ai);
        a.removePhoto(pi);
        Write();

    }

    public void movePhoto(int oldAlbum, int newAlbum, int pi){

        Album oldA = albums.get(oldAlbum);
        Album newA = albums.get(newAlbum);
        newA.addPhoto(oldA.getPhoto(pi));
        oldA.removePhoto(pi);
        Write();

    }

    public ArrayList<Photo> getPhotos(int ai){
        Album a = albums.get(ai);
        if (a != null) {
            return a.getPhotos();
        }
        return null;
    }

    public ArrayList<Photo> getResult(String type, ArrayList<String> hash){
        ArrayList<Photo> photos = new ArrayList<Photo>();
        for(int a = 0; a < albums.size(); a++){
            for(Photo p : albums.get(a).getPhotos()){
                for(Tag t : p.getTags()){
                    for(int h = 0; h < hash.size(); h++){
                        if(t.getType().equals(type)
                                && t.getValue().toLowerCase().trim().contains(hash.get(h).toLowerCase())){
                                if(photos.contains(p))
                                    photos.remove(p);
                            photos.add(p);
                        }
                    }
                }
            }
        }
        return photos;
    }

    public void addTag(int ai, int pi, String type, String value){
        Album a = albums.get(ai);
        a.getPhoto(pi).addTag(new Tag(type, value));
        Write();
    }

    public void removeTag(int ai, int pi, int ti){
        Album a = albums.get(ai);
        a.getPhoto(pi).removeTag(ti);
        Write();
    }

    public ArrayList<Tag> getTags(int ai, int pi){
        Album a = albums.get(ai);
        if(a != null){
            return a.getPhoto(pi).getTags();
        }
        return null;
    }


    public boolean albumExists(String album){
        for(Album a : albums){
            if(a.getAlbumName().toLowerCase().equals(album.toLowerCase()))
                return true;
        }
        return false;
    }

    public boolean tagExists(int ai, int pi, String type, String value){
        Album a = albums.get(ai);
        ArrayList<Tag> tags =  a.getPhoto(pi).getTags();
        for(Tag t : tags){
            if(t.getType().toLowerCase().equals(type) & t.getValue().toLowerCase().equals(value))
                return true;
        }
        return false;
    }

    private void Write() {

        try{

            FileOutputStream fos = context.openFileOutput(url, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(albums);
            oos.close();

        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }

    }

    private void Read() {
        try{

            FileInputStream fis = context.openFileInput(url);
            ObjectInputStream ois = new ObjectInputStream(fis);
            albums = (ArrayList<Album>) ois.readObject();

        }catch(FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }

    }


}
