package xyz.nurat.photoalbum;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Authors: Tarun Sreenathan & Xiaochen Li
 */

public class AlbumActivity extends AppCompatActivity {

    private ListView listView;
    private TextView none;
    private ArrayList<Album> albums;
    private ReadWriter rw;
    public static final int ADD_ALBUM_CODE = 1;
    public static final int EDIT_ALBUM_CODE = 2;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        none = (TextView)findViewById(R.id.none_albums);

        context = this;
        rw = ReadWriter.getInstance(context);
        albums = rw.getAlbums();

        view();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != RESULT_OK) { return; }

        Bundle bundle = data.getExtras();
        if (bundle == null) { return; }

        String name = bundle.getString(AlbumAddActivity.ALBUM_NAME);

        int i = bundle.getInt(AlbumAddActivity.ALBUM_INDEX);

        if(requestCode == EDIT_ALBUM_CODE){
            rw.renameAlbum(i, name);
        }else if(requestCode == ADD_ALBUM_CODE){
            rw.addAlbum(name);
        }

        view();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_searchadd, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                addAlbum();
                return true;
            case R.id.action_search:
                search();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void search() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    public void addAlbum(){
        Intent intent  = new Intent(this, AlbumAddActivity.class);
        startActivityForResult(intent, ADD_ALBUM_CODE);
    }

    public void view(){
        if(albums.isEmpty()){
            none.setText("No Albums Found");
        }else{
            none.setText("");
        }
        listView = (ListView)findViewById(R.id.albumList);
        AlbumAdapter aa = new AlbumAdapter(albums, this);
        listView.setAdapter(aa);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id){
               openAlbum(position);
            }
        });
    }


    private void openAlbum(int position) {

        Bundle bundle = new Bundle();
        Album a = albums.get(position);
        bundle.putInt(PhotosActivity.ALBUM_INDEX, position);
        bundle.putString(PhotosActivity.ALBUM_NAME, a.getAlbumName());
        Intent intent = new Intent(this, PhotosActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    public class AlbumAdapter extends BaseAdapter implements ListAdapter {
        private ArrayList<Album> list = new ArrayList<Album>();
        private Context context;

        public AlbumAdapter(ArrayList<Album> list, Context context) {
            this.list = list;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int pos) {
            return list.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;

            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.activity_cell, null);
            }

            //View Text
            TextView itemText = (TextView)view.findViewById(R.id.string_text);
            itemText.setText(list.get(position).getAlbumName());

            //Handle buttons and add onClickListeners
            Button deleteBtn = (Button)view.findViewById(R.id.delete_btn);
            Button editBtn = (Button)view.findViewById(R.id.edit_btn);

            deleteBtn.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(context)
                            .setTitle(list.get(position).getAlbumName())
                            .setMessage("Are you sure you want to delete ?")

                            .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    rw.removeAlbum(position);
                                    view();
                                }})

                            .setNegativeButton(android.R.string.no, null).show();
                }
            });

            editBtn.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();

                    Album album = albums.get(position);
                    bundle.putInt(AlbumAddActivity.ALBUM_INDEX,position);
                    bundle.putString(AlbumAddActivity.ALBUM_NAME, album.getAlbumName());

                    Intent intent = new Intent(context, AlbumAddActivity.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, EDIT_ALBUM_CODE);
                }
            });

            return view;
        }
    }

}
