package xyz.nurat.photoalbum;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * Authors: Tarun Sreenathan & Xiaochen Li
 */

public class Photo implements Serializable {

    private String uri;
    private String fileName;
    private ArrayList<Tag> tags = new ArrayList<Tag>();

    public Photo(String uri, String fileName){
        this.uri = uri;
        this.fileName = fileName;
        tags = new ArrayList<Tag>();
    }

    public ArrayList<Tag> getTags(){
        return tags;
    }

    public void addTag(Tag tag){
        tags.add(tag);
    }

    public void removeTag(int tag){
        tags.remove(tag);
    }

    public String getUri(){
        return uri;
    }

    public String getFileName(){
        return  fileName;
    }

    @Override
    public String toString() {   // used by ListView
        return uri;
    }
}
