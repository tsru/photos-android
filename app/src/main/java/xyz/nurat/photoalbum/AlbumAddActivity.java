package xyz.nurat.photoalbum;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


/**
 * Authors: Tarun Sreenathan & Xiaochen Li
 */

public class AlbumAddActivity extends AppCompatActivity {

    public static final String ALBUM_INDEX = "albumIndex";
    public static final String ALBUM_NAME = "albumName";
    private ReadWriter rw;
    private int albumIndex;
    private EditText albumName;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addalbum);

        context = this;
        rw = ReadWriter.getInstance(context);

        albumName = (EditText)findViewById(R.id.albumNameF);

        Bundle bundle = getIntent().getExtras();
        Button state = (Button) findViewById(R.id.addAlbum);

        if(bundle != null){
            albumIndex = bundle.getInt(ALBUM_INDEX);
            albumName.setText(bundle.getString(ALBUM_NAME));
            state.setText("Update");
            setTitle(getResources().getString(R.string.title_activity_editAlbum));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }


    public void addToList(View view){
        String name = albumName.getText().toString();

        if(rw.albumExists(name)){
            Messages.showDFMessage("Album Exists",name,"Album already exists",getFragmentManager());
            return;
        }

        if(name == null || name.length() == 0){
            Messages.showDFMessage("Missing Fields","","Name Required",getFragmentManager());
            return;
        }

        Bundle bundle = new Bundle();

        bundle.putInt(ALBUM_INDEX, albumIndex);
        bundle.putString(ALBUM_NAME, name);

        Intent intent = new Intent();
        intent.putExtras(bundle);

        setResult(RESULT_OK,intent);
        finish();
    }

}
