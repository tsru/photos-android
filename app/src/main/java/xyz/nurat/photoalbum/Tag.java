package xyz.nurat.photoalbum;

import java.io.Serializable;


/**
 * Authors: Tarun Sreenathan & Xiaochen Li
 */

public class Tag implements Serializable {

    private String type;
    private String value;

    public Tag(String type, String value){
        this.type = type;
        this.value = value;
    }

    public String getType(){
        return this.type;
    }

    public String getValue(){
        return this.value;
    }

    @Override
    public String toString() {
        return value;
    }
}
