package xyz.nurat.photoalbum;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Authors: Tarun Sreenathan & Xiaochen Li
 */

public class Album implements Serializable {

    private String name;
    private ArrayList<Photo> photos = new ArrayList<Photo>();

    public Album(String name){
        this.name = name;
        photos = new ArrayList<Photo>();
    }

    public String getAlbumName(){
        return this.name;
    }

    public void setAlbumName(String name){
        this.name = name;
    }

    public ArrayList<Photo> getPhotos(){
        return photos;
    }

    public void addPhoto(Photo photo){
        photos.add(photo);
    }

    public void removePhoto(int i){
        photos.remove(i);
    }

    public Photo getPhoto(int i){
        return photos.get(i);
    }

    @Override
    public String toString() {
        return name;
    }

}
